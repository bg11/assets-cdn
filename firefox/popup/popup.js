let bg = browser.extension.getBackgroundPage();
global = bg.get_global();

let redirect = {
    name: "重定向",
    data: []
};
let cancel = {
    name: "屏蔽",
    data: []
};
for (let site in global.chart.redirect) {
    redirect.data.push({
        "name": site,
        "y": global.chart.redirect[site]
    });
}
for (let site in global.chart.cancel) {
    cancel.data.push({
        "name": site,
        "y": global.chart.cancel[site]
    });
}
$("#chart").highcharts({
    chart: {
        type: "column"
    },
    title: {
        text: "统计信息"
    },
    xAxis: {
        type: "category"
    },
    yAxis: {
        title: {
            text: "加速次数"
        },
        stackLabels: {
            enabled: true
        }
    },
    plotOptions: {
        column: {
            stacking: "normal"
        }
    },
    series: [redirect, cancel]
});